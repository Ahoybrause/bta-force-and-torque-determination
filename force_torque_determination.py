import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from nptdms import TdmsFile

time_begin = 104 # timestamp beginn process in tdms
time_end = 235.5 # timestamp end process in tdms
reduce_factor = 75 # keep elements per second for visualization
filename = "MABR10A"
print_statistics = 0 #1 to print statistics

filename_input_Dehnung = "Dehnung" + ".tdms"
filename_input_niose = "Schalldruck" + ".tdms"
output_filename_axialforce = filename + "_axialforce.csv"
output_filename_torque = filename + "_torque.csv"
output_filename_axialforce_fft = filename + "_axialforce_fft.csv"
output_filename_torque_fft = filename + "_torque_fft.csv"
output_filename_noise_fft = filename + "_noise_fft.csv"
tdms_file_Dehnung = TdmsFile.read(filename_input_Dehnung)
tdms_file_niose = TdmsFile.read(filename_input_niose)

sample_rate = 25000

# Load TDMS data force and torque
def load_tdms():
    tdms_groups = tdms_file_Dehnung.groups()
    select_group = str(tdms_groups[0])

    char1 = "/"
    char2 = ">"
    select_group = select_group[select_group.find(char1)+2 : select_group.find(char2)-1]

    group = tdms_file_Dehnung[select_group]
    torque_strain = group['cDAQ7Mod1_ai0']
    axialforce_strain = group['cDAQ7Mod1_ai1']
    torque_strain = torque_strain[:]
    axialforce_strain = axialforce_strain[:]
    return axialforce_strain, torque_strain

# Load TDMS data noise
def load_tdms_noise():
    tdms_groups = tdms_file_niose.groups()
    select_group = str(tdms_groups[0])

    char1 = "/"
    char2 = ">"
    select_group = select_group[select_group.find(char1)+2 : select_group.find(char2)-1]

    group = tdms_file_niose[select_group]
    noise = group['cDAQ7Mod2_ai0']
    noise = noise[:]
    return noise

# Read statistics for force and torque
def get_box_plot_data(labels, boxplot, axialforce_cut, torque_cut):
    rows_list = []
    min = [np.amin(axialforce_cut), np.amin(torque_cut)]
    max = [np.amax(axialforce_cut), np.amax(torque_cut)]

    for i in range(len(labels)):
        dict = {}
        dict['label'] = labels[i]
        dict['minimum'] = min[i]
        dict['maximum'] = max[i]
        dict['mean'] = boxplot['means'][i].get_ydata()[0]
        dict['median'] = boxplot['medians'][i].get_ydata()[1]
        dict['lower_quartile'] = boxplot['boxes'][i].get_ydata()[1]
        dict['upper_quartile'] = boxplot['boxes'][i].get_ydata()[2]
        dict['lower_whisker'] = boxplot['whiskers'][i*2].get_ydata()[1]
        dict['upper_whisker'] = boxplot['whiskers'][(i*2)+1].get_ydata()[1]
        rows_list.append(dict)

    return pd.DataFrame(rows_list)

# Read data out of the important time intervall
def interval(axialforce, torque, noise):
    time = np.linspace(0, len(axialforce)-1, len(axialforce))
    mask1 = time > time_begin * sample_rate
    mask2 = time < time_end * sample_rate
    return axialforce[mask1 & mask2], torque[mask1 & mask2], noise[mask1 & mask2], time[mask1 & mask2]

# Data reduction for accelerated rendering
def reduce_data(data, sample_rate, factor):
    x = int(sample_rate / factor)
    data = data[::x]
    return data

# FFT
def fft(data):
    fft_data = np.fft.fft(data)
    return fft_data

axialforce_strain, torque_strain = load_tdms()
noise = load_tdms_noise()

axialforce = axialforce_strain * 257594599.4 # Convert strain to force
torque = torque_strain * -2873387.56 # Convert strain to torque

axialforce_cut, torque_cut, noise_cut, timesteps = interval(axialforce, torque, noise) # Data out of process time
time = timesteps / sample_rate

#print statistics
if print_statistics == 1:
    labels = ['axialforce', 'torque']
    boxplot = plt.boxplot([axialforce_cut, torque_cut], labels=labels, showmeans=True)
    print(get_box_plot_data(labels, boxplot, axialforce_cut, torque_cut))
    fig, ax = plt.subplots()
    ax.violinplot = plt.violinplot([axialforce_cut, torque_cut])

#plot axialforce and torque
#plt.plot(time, axialforce_cut)
#plt.plot(time, torque_cut)
#plt.show()

#FFT axialforce
fft_axialforce = np.abs(fft(axialforce_cut))
N = int(len(fft_axialforce)/2)
X = np.linspace(0, sample_rate/2, N, endpoint=True)

fft_axialforce = np.abs(2*fft_axialforce[:N]/N)
df_fft_axialforce = pd.DataFrame(list(zip(X,fft_axialforce)), columns = ['Frequency','Amplitude'])
df_fft_axialforce = df_fft_axialforce[(df_fft_axialforce.Frequency < 3000) & (df_fft_axialforce.Amplitude > 5)]
df_fft_axialforce = df_fft_axialforce.append({'Frequency':3000, 'Amplitude':0}, ignore_index=True)
df_fft_axialforce = df_fft_axialforce.round({'Frequency': 3, 'Amplitude': 1})

X_axialforce = df_fft_axialforce['Frequency'].tolist() #create list out of pandas dataframe
fft_axialforce = df_fft_axialforce['Amplitude'].tolist() #create list out of pandas dataframe

#FFT torque
fft_torque = np.abs(fft(torque_cut))
fft_torque = np.abs(2*fft_torque[:N]/N)

df_fft_torque = pd.DataFrame(list(zip(X,fft_torque)), columns = ['Frequency','Amplitude'])
df_fft_torque = df_fft_torque[(df_fft_torque.Frequency < 3000) & (df_fft_torque.Amplitude > 1)]
df_fft_torque = df_fft_torque.append({'Frequency':3000, 'Amplitude':0}, ignore_index=True)
df_fft_torque = df_fft_torque.round({'Frequency': 3, 'Amplitude': 1})

X_torque = df_fft_torque['Frequency'].tolist() #create list out of pandas dataframe
fft_torque = df_fft_torque['Amplitude'].tolist() #create list out of pandas dataframe

#FFT noise
# N = int(len(fft_noise)/2)

fft_noise = np.abs(fft(noise_cut))
fft_noise = np.abs(2*fft_noise[:N]/N)

df_fft_noise = pd.DataFrame(list(zip(X,fft_noise)), columns = ['Frequency','Amplitude'])
df_fft_noise.Amplitude = np.where(df_fft_noise.Amplitude >= 0.05, df_fft_noise.Amplitude * 1000, df_fft_noise.Amplitude) # filter noise
df_fft_noise = df_fft_noise[(df_fft_noise.Frequency < 3000) & (df_fft_noise.Amplitude > 0.008)]
df_fft_noise = df_fft_noise.append({'Frequency':3000, 'Amplitude':0}, ignore_index=True)
df_fft_noise = df_fft_noise.round({'Frequency': 3, 'Amplitude': 0})

X_noise = df_fft_noise['Frequency'].tolist() #create list out of pandas dataframe
fft_noise = df_fft_noise['Amplitude'].tolist() #create list out of pandas dataframe

#fft_noise = 20 / 1000 * np.log10(fft_noise / (2 * 10 ** (-5))) # convert noise from mPa to dB

#plot FFT
#plt.plot(X_axialforce, fft_axialforce)
#plt.plot(X_torque, fft_torque)
#plt.plot(X_noise, fft_noise)
#plt.xlabel('Frequency ($Hz$)')
#plt.ylabel('Amplitude')
#plt.show()

# reducing data for accelarated visualization
axialforce_plot = reduce_data(axialforce_cut, sample_rate, reduce_factor)
torque_plot = reduce_data(torque_cut, sample_rate, reduce_factor)
time_plot = reduce_data(time, sample_rate, reduce_factor)

# create .csv
result = pd.DataFrame({
    "time": time_plot,
    "axialforce_cut": axialforce_plot
})
result.index += 1
result.to_csv(output_filename_axialforce, index=False, header=False, sep="\t",)

result1 = pd.DataFrame({
    "time": time_plot,
    "torque_cut": torque_plot
})
result1.index += 1
result1.to_csv(output_filename_torque, index=False, header=False, sep="\t",)

result2 = pd.DataFrame({
    "Frequency": X_axialforce,
    "fft_axialforce": fft_axialforce
})
result2.index += 1
result2.to_csv(output_filename_axialforce_fft, index=False, header=False, sep="\t",)

result3 = pd.DataFrame({
    "Frequency": X_torque,
    "fft_torque": fft_torque
})
result3.index += 1
result3.to_csv(output_filename_torque_fft, index=False, header=False, sep="\t",)

result4 = pd.DataFrame({
    "Frequency": X_noise,
    "fft_noise": fft_noise
})
result4.index += 1
result4.to_csv(output_filename_noise_fft, index=False, header=False, sep="\t",)