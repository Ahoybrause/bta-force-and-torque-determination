import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from nptdms import TdmsFile
mpl.rc('text', usetex=True)
import pandas as pd

input_filename = "Schalldruck.tdms"
tdms_file = TdmsFile.read(input_filename)

sampling_frequency = 25000

# Load TDMS data
def load_tdms():
    tdms_groups = tdms_file.groups()
    select_group = str(tdms_groups[0])

    char1 = "/"
    char2 = ">"
    select_group = select_group[select_group.find(char1)+2 : select_group.find(char2)+-1]

    group = tdms_file[select_group]
    channel = group['cDAQ7Mod2_ai0']
    channel_data = channel[:]
    return channel_data

def reduce_data(data, sample_rate, factor):
    x = int(sample_rate / factor)
    data = data[::x]
    return data

def cut_data(t, data, min, max):
    df = pd.DataFrame((zip(t, data)),  columns = ['t','Schalldruck'])
    print(len(df))
    df = df[(df.t > min) & (df.t < max)]
    print(len(df))
    t = df['t'].tolist()
    data = df['Schalldruck'].tolist()
    return t, data

Schalldruck = load_tdms()
dt = 1/sampling_frequency
begin = 0
end = len(Schalldruck)/sampling_frequency
print(end)
t = np.arange(begin, end, dt)
t = np.round(t, 5)
Schalldruck = reduce_data(Schalldruck, sampling_frequency, 75)
t = reduce_data(t, sampling_frequency, 75)
print("Zeitstempel Anfang:")
min = float(input())
print("Zeitstempel Ende:")
max = float(input())
t, Schalldruck = cut_data(t, Schalldruck, min, max)
print("Versuchsnummer:")
num = input()

# create .csv
result = pd.DataFrame({
    "time": t,
    "Schalldruck": Schalldruck
})
result.index += 1
result.to_csv("MABR" + num + "_noise.csv", index=False, header=False, sep="\t",)