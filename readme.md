# Force and Torque analysis for BTA deep-hole drilling
The script analyzes the recorded forces and torque for BTA deep-hole drilling.
In addition, the recorded sound pressure is evaluated.
The data can be filtered.
The statistical data can be printed.
In addition, the FFT of the forces, moments and the sound pressure is performed.
The data is printed as .csv files for displaying the curves and FFT in various software.

## Required packages and installations
- Python3
- numpy
- pandas
- nptdms
- matplotlib

## Variables
|Variable|Significance|
|-----|-----|
|time_begin|Point of measurement where the process starts|
|time_end|Point of measurement where the process ends|
|reduce_factor|Factor for data reduction for plotting in LaTeX pgflpots without memory overflow|
|filename|Basic Output Filename|
|print_statistics|1 for printing statistics for Boxplots|
|sample_rate|sample rate in process|